ocamlmakefile (6.39.2-3) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints

 -- Stéphane Glondu <glondu@debian.org>  Mon, 14 Aug 2023 08:54:25 +0200

ocamlmakefile (6.39.2-2) unstable; urgency=low

  [ Stéphane Glondu ]
  * Team upload
  * Remove Samuel from Uploaders
  * Bump Standards-Version to 4.6.2
  * Add Rules-Requires-Root: no

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Stéphane Glondu <glondu@debian.org>  Sat, 12 Aug 2023 11:00:12 +0200

ocamlmakefile (6.39.2-1) unstable; urgency=medium

  * New upstream version
  * Update debian/watch to github tags
  * Update homepage to github
  * Update Vcs-{Git,Browser} to salsa
  * Change Priority from extra to optional
  * Build-depend on debian-compat, remove file debian/compat
  * Debhelper-compatibility level 12
  * debian/copyright:
    - use https in format header
    - update copyright years
  * Standards-Version 4.4.1 (no change)
  * Add an as-installed test

 -- Ralf Treinen <treinen@debian.org>  Tue, 10 Dec 2019 20:45:41 +0100

ocamlmakefile (6.37.0-3) unstable; urgency=medium

  [Remi Vanicat]
  * Removing myself (Rémi Vanicat) from uploaders

  [Ralf Treinen]
  * Debhelper compatibility level 10
  * Standards-Version 3.9.8 (no change)
  * Vcs-*: use secure URI
  * Drop Replaces/Conflicts to 10 year-old version of ocaml-tools
  * Update Homepage

 -- Ralf Treinen <treinen@debian.org>  Tue, 24 Jan 2017 03:23:38 +0100

ocamlmakefile (6.37.0-2) unstable; urgency=low

  * Upload to unstable.

 -- Ralf Treinen <treinen@debian.org>  Thu, 09 May 2013 20:20:26 +0200

ocamlmakefile (6.37.0-1) experimental; urgency=low

  * New upstream release
  * Standards-Version 3.9.4 (no change)
  * Migrate debian/rules to dh(1):
    - debian/rules: use dh
    - debian/control: bump version in build-dependency on debhelper
    - debian/rules: build modified version of OCamlMakefile and of the examples
      in temporary directory builddir.
    - add files debian/ocamlmakefile.{dirs,install,examples,docs} to install
      stuff.

 -- Ralf Treinen <treinen@debian.org>  Thu, 11 Oct 2012 08:58:20 +0200

ocamlmakefile (6.36.0-2) unstable; urgency=low

  * debian/control: updated project homepage
  * debian/watch, debian/copyright: new upstream repository at bitbucket.org

 -- Ralf Treinen <treinen@debian.org>  Fri, 08 Jun 2012 07:15:32 +0200

ocamlmakefile (6.36.0-1) unstable; urgency=low

  * New upstream release.
  * Standards-Version 3.9.3
    - debian/policy: convert to machine-readable format 1.0

 -- Ralf Treinen <treinen@debian.org>  Mon, 05 Mar 2012 08:32:21 +0100

ocamlmakefile (6.33.0-1) unstable; urgency=low

  [ Ralf Treinen ]
  * New upstream version.
  * debian/rules: renamed target "buildstamp" to "build-stamp", added
    targets build-{arch,indep}.
  * debian/control:
    - do not start short description on an article
    - standards version 3.9.2 (no change)

  [ Stefano Zacchiroli ]
  * remove myself from Uploaders

 -- Ralf Treinen <treinen@debian.org>  Tue, 27 Dec 2011 09:45:53 +0100

ocamlmakefile (6.30.0-1) unstable; urgency=low

  * New upstream release
  * Standards-Version 3.9.1 (no change)

 -- Ralf Treinen <treinen@debian.org>  Mon, 28 Mar 2011 21:02:09 +0200

ocamlmakefile (6.29.3-2) unstable; urgency=low

  [ Stephane Glondu ]
  * Switch packaging to git

  [ Ralf Treinen ]
  * Added dependency on ${misc:Depends}
  * debian/copyright: Removed special licence for debian packaging.
  * Standards-Version 3.8.3 (no change)
  * Source format 3.0 (quilt)

 -- Ralf Treinen <treinen@debian.org>  Sun, 03 Jan 2010 12:36:29 +0100

ocamlmakefile (6.29.3-1) unstable; urgency=low

  [ Samuel Mimram ]
  * Changed section to ocaml.
  * Update Homepage, closes: #528954.

  [ Ralf Treinen ]
  * New upstream version.
  * Standards-Version 3.8.1 (no change)
  * dh_installchangelogs: install Changelog instead of Changes
  * DH compat level 7; update build deps; dh_clean -k => dh_prep
  * debian/copyright: point to versioned LGPL file; update years of
    upstream copyright.

 -- Ralf Treinen <treinen@debian.org>  Sun, 17 May 2009 09:08:34 +0200

ocamlmakefile (6.29.1-1) unstable; urgency=low

  * New upstream release
  * Adapt debian/rules to the fact that upstream has renamed README to
    README.txt

 -- Ralf Treinen <treinen@debian.org>  Tue, 27 Jan 2009 08:06:54 +0100

ocamlmakefile (6.29.0-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Thu, 20 Nov 2008 07:40:35 +0100

ocamlmakefile (6.28.2-1) unstable; urgency=low

  * New upstream release.

 -- Ralf Treinen <treinen@debian.org>  Thu, 23 Oct 2008 20:11:27 +0200

ocamlmakefile (6.28.1-1) unstable; urgency=low

  * New upstream release.
  * debian/control: added Homepage field
  * Standards-version 3.8.0 (no change)

 -- Ralf Treinen <treinen@debian.org>  Wed, 20 Aug 2008 22:28:45 +0200

ocamlmakefile (6.28.0-1) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Samuel Mimram ]
  * New upstream release.
  * Updated standards version, no changes needed.

 -- Samuel Mimram <smimram@debian.org>  Tue, 29 Apr 2008 23:20:15 +0200

ocamlmakefile (6.27.0-1) unstable; urgency=low

  * New upstream release.
  * Adds a debug-native-code target, closes: #449451.

 -- Samuel Mimram <smimram@debian.org>  Wed, 28 Nov 2007 23:47:46 +0000

ocamlmakefile (6.26.0-1) unstable; urgency=low

  * New upstream release.
  * Added ocaml-nox to recommends.
  * Removed Sven Luther from uploaders.

 -- Samuel Mimram <smimram@debian.org>  Mon, 15 Oct 2007 18:42:56 +0000

ocamlmakefile (6.25.0-1) unstable; urgency=low

  * New upstream release. This version has new targets libinstall-byte-code
    and libinstall-native-code (closes: Bug#381638).

 -- Ralf Treinen <treinen@debian.org>  Mon, 20 Aug 2007 17:42:53 +0200

ocamlmakefile (6.24.8-2) unstable; urgency=low

  * Removed trailing comma from the Uploaders field (closes: Bug#426487)

 -- Ralf Treinen <treinen@debian.org>  Wed, 04 Jul 2007 21:16:02 +0200

ocamlmakefile (6.24.8-1) unstable; urgency=low

  * Initial release (Closes: Bug#425293). This package is a split off from
    the ocaml-tools package.

 -- Ralf Treinen <treinen@debian.org>  Fri, 18 May 2007 22:22:08 +0200
